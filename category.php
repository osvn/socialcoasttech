<?php
global $osvn_opt;
get_header();?>
<div class="main">
            <div class="page-caption">
                <div class="container">
                    <div class="caption">elements® <big><?php single_cat_title();?></big></div>
                </div>
            </div>
            <!-- /.page-caption -->
            <div class="container">
                <div class="row">
                    <div class="main-content col-md-8">
                        <div id="columns2" class="row columns" data-columns>
                            <?php $i=0;while(have_posts()): the_post();?>
                            <div id="post-<?php echo $i;?>" <?php echo post_class();?>>
                                <a href="<?php the_permalink();?>"><?php the_post_thumbnail('home-blog');?></a>
                                <p class="news-date"><?php the_time('M j');?></p>
                                <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                                <div>
                                    <?php 
                                    if ( !empty( $post->post_excerpt ) ) :
                                        the_excerpt();
                                    else :
                                        echo wp_trim_words($post->post_content, 50);
                                    endif;
                                    ?>
                                </div>
                                <a href="<?php the_permalink();?>" class="button">read more</a>
                            </div>
                            <!-- /.news-item -->
                            <?php $i++;endwhile;?>
                        </div>
                        <!-- /.columns -->
                        <div class="row infinite_scroll_render">
                            <div class="col-sm-6 col-sm-offset-6 text-center">
                                <a id="infinite-handle" href="#" class="load-more-post">load more post</a>
                                <a href="#" class="back-to-top">back to top <i class="fa fa-chevron-up"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- /.main-content -->
                    <?php get_sidebar();?>
                </div>
            </div>
        </div>
        <!-- /.main -->
        <?php 
        global $wp_query;
        if ( $wp_query->max_num_pages <= 1 ) {
            echo '<style>#infinite-handle{display:none;}</style>';
        }
        ?>
<?php get_footer();?>