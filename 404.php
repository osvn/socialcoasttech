<?php
get_header();
global $osvn_opt;
?>
        <div class="main">
            <div class="page-caption">
                <div class="container">
                    <div class="caption">elements® <big>Not Found</big></div>
                </div>
            </div>
            <!-- /.page-caption -->
            <div class="container">
                <div class="row">
                    <div class="main-content col-md-8">
                        <div id="columns2" class="row columns" data-columns>
                           <?php get_search_form();?>
                        </div>
                        <!-- /.columns -->
                    </div>
                    <!-- /.main-content -->
                    <?php get_sidebar();?>
                </div>
            </div>
        </div>
        <!-- /.main -->
<?php get_footer();?>