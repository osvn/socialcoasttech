<?php
//https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress/wiki
function osvn_metaboxes( $meta_boxes ) {
    $prefix = '_osvn_'; // Prefix for all fields
    /////////////////////////////////////////
    $tt_pages = array();
    $tt_pages_obj = get_pages('sort_column=post_parent,menu_order');    
    foreach ($tt_pages_obj as $tt_page) {
    $tt_pages[$tt_page->ID] = $tt_page->post_title; }
    /////////////////////////////////////////
    $tt_terms = array();
    $tt_terms_obj = get_terms('category');    
    foreach ($tt_terms_obj as $tt_term) {
    $tt_terms[$tt_term->term_id] = $tt_term->name; }
    
    
    
    
    $meta_boxes['_home_slider'] = array(
        'id'         => '_home_slider',
        'title'      => __( 'Section #1: slider', 'cmb' ),
        'pages'      => array( 'page', ),
        'show_on' => array( 'key' => 'page-template', 'value' => 'Template-home.php' ),
        'fields'     => array(
            array(
                'id'          => $prefix . 'home_slider',
                'type'        => 'group',
                'description' => __( 'Add', 'cmb' ),
                'options'     => array(
                    'group_title'   => __( 'Box {#}', 'cmb' ), // {#} gets replaced by row number
                    'add_button'    => __( 'Add', 'cmb' ),
                    'remove_button' => __( 'Remove', 'cmb' ),
                    'sortable'      => true, // beta
                ),
                // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
                'fields'      => array(


                    array(
                        'name'    => 'Title',
                        //'desc'    => 'Select an option',
                        'id'      => $prefix . 'home1_title',
                        'type'    => 'text',
                    ),
                    array(
                        'name' => 'Image',
                        'desc' => 'Upload an image or enter an URL.',
                        'id' => $prefix . 'home1_image',
                        'type' => 'file',
                        'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                    ),
                    
                ),
            ),
        ),
    );
    $meta_boxes[] = array(
        'id' => 'osvn_metabox_home2',
        'title' => __( 'Section #2: Product Info', 'osvn' ),
        'pages' => array('page'), // post type
        'show_on' => array( 'key' => 'page-template', 'value' => 'Template-home.php' ),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            array(
                'name' => 'Content',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'home2_des',
                'type' => 'wysiwyg'
            ),
            array(
                'name' => 'Video iframe',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'home2_video',
                'type' => 'textarea_code'
            ),
            
            
            
        ),
    );
    $meta_boxes['_home3'] = array(
        'id'         => '_home3',
        'title'      => __( 'Section #3: The Team', 'cmb' ),
        'pages'      => array( 'page', ),
        'show_on' => array( 'key' => 'page-template', 'value' => 'Template-home.php' ),
        'fields'     => array(
            array(
                'id'          => $prefix . 'home3',
                'type'        => 'group',
                'description' => __( 'Add', 'cmb' ),
                'options'     => array(
                    'group_title'   => __( 'Box {#}', 'cmb' ), // {#} gets replaced by row number
                    'add_button'    => __( 'Add', 'cmb' ),
                    'remove_button' => __( 'Remove', 'cmb' ),
                    'sortable'      => true, // beta
                ),
                // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
                'fields'      => array(


                    array(
                        'name'    => 'Name',
                        //'desc'    => 'Select an option',
                        'id'      => $prefix . 'home3_name',
                        'type'    => 'text',
                    ),
                    array(
                        'name'    => 'Position',
                        //'desc'    => 'Select an option',
                        'id'      => $prefix . 'home3_position',
                        'type'    => 'text',
                    ),
                    array(
                        'name' => 'Image',
                        'desc' => 'Upload an image or enter an URL.',
                        'id' => $prefix . 'home3_image',
                        'type' => 'file',
                        'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                    ),
                    
                ),
            ),
        ),
    );
    
    $meta_boxes[] = array(
        'id' => 'osvn_metabox_home4',
        'title' => __( 'Section #4: Our Mission', 'osvn' ),
        'pages' => array('page'), // post type
        'show_on' => array( 'key' => 'page-template', 'value' => 'Template-home.php' ),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            
            
            array(
                'name' => 'Content',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'home4_des',
                'type' => 'wysiwyg'
            ),
            
            
            
        ),
    );
    $meta_boxes[] = array(
        'id' => 'osvn_metabox_home5',
        'title' => __( 'Section #5: Latest News', 'osvn' ),
        'pages' => array('page'), // post type
        'show_on' => array( 'key' => 'page-template', 'value' => 'Template-home.php' ),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            
            
            array(
                'name' => 'Choose category',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'home5_cat',
                'type' => 'select',
                'options' => $tt_terms,
            ),
            
            
            
        ),
    );
    
    
    return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'osvn_metaboxes' );