<?php 
add_action('init','osvn_client_post_type');
	function osvn_client_post_type() {

		$labels = array(

		    'name' => 'Sliders',

		    'singular_name' => 'Sliders',

		    'add_new' => 'Add slider',

		    'all_items' => 'Sliders',

		    'add_new_item' => 'Add slider',

		    'edit_item' => 'Edit slider',

		    'new_item' => 'Add slider',

		    'view_item' => 'View slider',

		    'search_items' => 'Search slider',

		    'not_found' =>  'Not found slider',

		    'not_found_in_trash' => 'Not found slider in trash',

		    'parent_item_colon' => 'Parent slider: ',

		    'menu_name' => 'Sliders'

		);

		

		$args = array(

			'labels' => $labels,

			'description' => "",

			'public' => true,

			'exclude_from_search' => false,

			'publicly_queryable' => true,

			'show_ui' => true, 

			'show_in_nav_menus' => true, 

			'show_in_menu' => true,

			'show_in_admin_bar' => true,

			//'menu_position' => 100,

			'menu_icon' => 'dashicons-images-alt2',

			//'capability_type' => 'post',

			'hierarchical' => false,

			'supports' => array('title', 'thumbnail'),

			'has_archive' => false,

			//'rewrite'   => array( 'slug' => 'p-showroom' ),

			'query_var' => true,

			'can_export' => true

		); 

		

		register_post_type('osvn_slide',$args);

	}
	///////
	//price range
	register_taxonomy("osvn_price", 
		array('product'), 
		array(	"hierarchical" => true, 
		"label" => "Price range", 
		"singular_label" => "Price range", 
		'rewrite'   => array( 'slug' => 'product-price-range' ),
		"query_var" => true
	));
	//Color
	register_taxonomy("osvn_color", 
		array('product'), 
		array(	"hierarchical" => true, 
		"label" => "Colors", 
		"singular_label" => "Colors", 
		'rewrite'   => array( 'slug' => 'product-color' ),
		"query_var" => true
	));
?>