<?php
function osvn_custom_dashboard_widgets() {
	global $wp_meta_boxes;

	wp_add_dashboard_widget( 'custom_help_widget', 'Welcome', 'custom_dashboard_help' );
}

function custom_dashboard_help() {
	?>
	<p class="about-description"><?php _e( 'This website was designed and developed by ' ); ?><a
			href="http://creativehaus.com/" target="_blank"><?php echo _e( 'Creativehaus.com' ); ?></a></p>
	<p></p>
	<p><a href="http://creativehaus.com/" target="_blank"><img
				src="<?php echo OSVN_IMG . '/logo-creativehaus.png'; ?>"></a></p>
	<p></p>
	<div class="">
		<p class="about-description">If you are currently on, or interested in maintenance, please contact us.</p>

		<p><strong>Contact Number: 1-888-966-7265. Support:</strong> <a href="http://creativehaus.com/contact/"
		                                                                target="_blank">Click here</a></p>
	</div>
<?php
}

function osvn_title() {
	global $paged, $s;
	if ( ! function_exists( 'wpseo_init' ) ) {
		if ( function_exists( 'is_tag' ) && is_tag() ) {
			single_tag_title( __( 'Tag Archive for &quot;', 'osvn' ) );
			_e( '&quot; - ', 'osvn' );
		} elseif ( is_archive() ) {
			wp_title();
			_e( ' Archive - ', 'osvn' );
		} elseif ( is_search() ) {
			_e( 'Search for &quot;', 'osvn' ) . htmlspecialchars( $s ) . _e( '&quot; - ', 'osvn' );
		} elseif ( ! ( is_404() ) && ( is_single() ) || ( is_page() ) ) {
			wp_title();
			_e( ' - ', 'osvn' );
		} elseif ( is_404() ) {
			_e( 'Not Found - ', 'osvn' );
		}

		if ( is_home() ) {
			bloginfo( 'name' );
			_e( ' - ', 'osvn' );
			bloginfo( 'description' );
		} else {
			bloginfo( 'name' );
		}

		if ( $paged > 1 ) {
			_e( ' - page ', 'osvn') . $paged; }
	} else {
		wp_title();
	}
}
/*=================logo=============================*/
function osvn_logo(){
    global $osvn_opt;
    echo '<a  class="anchor" href="'.esc_url(home_url('/')).'">';
            if(isset($osvn_opt['logo']['url']) && !empty($osvn_opt['logo']['url'])){
                echo '<img src="'.$osvn_opt['logo']['url'].'" alt="'.get_bloginfo('name').'">';
            }else{
                echo '<img src="'.OSVN_IMG.'/logo.jpg" alt="'.get_bloginfo('name').'">';
            }
    echo '</a>';
}

/*=================social=============================*/
function osvn_social(){
    global $osvn_opt;
    echo '<ul class="social">';
    	if(! empty($osvn_opt['osvn-facebook-link'])){echo '<li><a href="'.$osvn_opt['osvn-facebook-link'].'" target="_blank" class="social fb">Facebook</a></li>';}
        if(! empty($osvn_opt['osvn-twitter-link'])){echo '<li><a href="'.$osvn_opt['osvn-twitter-link'].'" target="_blank" class="social tw">twitter</a></li>';}
        if(! empty($osvn_opt['osvn-youtube-link'])){echo '<li><a href="'.$osvn_opt['osvn-youtube-link'].'" target="_blank" class="social yt">youtube</a></li>';}
        if(! empty($osvn_opt['osvn-instagram-link'])){echo '<li><a href="'.$osvn_opt['osvn-instagram-link'].'" target="_blank" class="social in">instagram</a></li>';}
    echo '</ul>';
}
/*=================phone=============================*/
function osvn_hotline(){
	global $osvn_opt;
	if(isset($osvn_opt['hotline']) && !empty($osvn_opt['hotline'])){
		$string = $osvn_opt['hotline'];
		$pattern = '/[^0-9]*/';
		$result = preg_replace($pattern,'', $string);
		echo '<a class="hotline" href="tel:'.$result.'"><img src="'.OSVN_IMG.'/phone12.png" alt="img" />'.$string.'</a>';
	}
}
/*====================add class================================*/
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');
function posts_link_attributes() {
    return 'class="next"';
}

/*===========filter tags cloud args==============*/
function custom_tag_cloud_widget() {
    $args = array(
        'separator' => ", ", 
    );
    return $args;
}
add_filter( 'widget_tag_cloud_args', 'custom_tag_cloud_widget' );
?> 