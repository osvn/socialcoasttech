<?php
global $osvn_opt;
get_header();
?>

        <div class="main">
            <div class="container">
                    <?php 
                    if ( have_posts() ) // Neu co bai viet
                    {
                        while ( have_posts() ) : the_post();

                            get_template_part( 'content' );

                        endwhile;
                    } else  // Neu khong co bai viet
                    {
                        get_template_part( 'content', 'none' );
                    }
                    ?>
            </div>
        </div>
        <!-- /.hotshot -->
<?php
get_footer();?>