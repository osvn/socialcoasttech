<?php
/*
Template name: Home
*/
?>
<?php get_header();?>
<?php global $post;?>
    <div id="slider">
        <div class="container">
            <div class="back"></div>
            <div class="content">
                <?php 
                if($post->post_content){
                    echo apply_filters('the_content', $post->post_content);
                }
                ?>
            </div><!--/.content -->
            <?php 
            $sec1 = get_post_meta(get_the_ID(), '_osvn_home_slider', true);
            if(isset($sec1) && !empty($sec1)){
            ?>
            <div class="flexslider absolute">
                <ul class="slides">
                    <?php 
                    foreach ( (array) $sec1 as $key => $entry ) {
                    if ( isset( $entry['_osvn_home1_image_id'] ) ) { 
                    ?>
                    <li>
                        <?php 
                        echo wp_get_attachment_image( $entry['_osvn_home1_image_id'], 'home-slider', null, array(
                                            'class' => 'res-img',
                                        ) );
                        ?>
                    </li>
                    <?php }}?>
                </ul>
            </div><!--.flexslider-->
            <?php }?>

        </div><!--/.container -->
    </div><!--/#slider -->
    
    <?php 
    $sec2 = get_post_meta(get_the_ID(), '_osvn_home2_des', true);
    if(isset($sec2) && !empty($sec2)){
    ?>
    <div id="product-info">
        <div class="container">
            <div class="left fl">
                <?php if($video = get_post_meta(get_the_ID(), '_osvn_home2_video', true)){
                    echo $video;
                }
                ?>
            </div>
            <div class="right fr">
                <?php echo wpautop($sec2);?>
            </div>
        </div>
    </div><!--/product-info -->
    <?php }?>
    
    <?php 
    $sec3 = get_post_meta(get_the_ID(), '_osvn_home3', true);
    if(isset($sec3) && !empty($sec3)){
    ?>
    <div id="the-team">
        <div class="container">
            <h1 class="text-center">The Social Coast Tech Team</h1>
            <ul class="cols">
                <?php foreach ( (array) $sec3 as $key => $entry ) {?>
                <li>
                    <?php 
                    if ( isset( $entry['_osvn_home3_image_id'] ) ) { 
                       echo wp_get_attachment_image( $entry['_osvn_home3_image_id'], 'home-team', null, array(
                                            'class' => 'avatar res-img',
                                        ) ); 
                    }
                    ?>
                    <h4><?php echo $entry['_osvn_home3_name'];?> </h4>
                    <p><?php echo $entry['_osvn_home3_position'];?></p>
                </li>
                <?php }?>
            </ul>
        </div>
    </div><!--/#the-team -->
    <?php }?>
    
    <?php 
    $sec4 = get_post_meta(get_the_ID(), '_osvn_home4_des', true);
    if(isset($sec4) && !empty($sec4)){
    ?>
    <div id="our-mission">
        <div class="container">
            <img src="<?php echo OSVN_IMG;?>/iphone.jpg" alt="" class="res-img banner">
            <div class="content">
                <img src="<?php echo OSVN_IMG;?>/homless.jpg" alt="" class="avatar res-img">
                <?php echo wpautop($sec4);?>
            </div>
        </div>
    </div><!--/#our-mission -->
    <?php }?>
    
    <?php 
    $sec4 = get_post_meta(get_the_ID(), '_osvn_home5_cat', true);
    if(isset($sec4) && !empty($sec4)){
    ?>
    <div id="news">
        <div class="container">
            <h1>Latest News</h1>
            <ul class="row">
                <?php 
                $args = array('post_type'=>'post','cat'=>$sec4, 'posts_per_page'=>3);
                $wp = new WP_Query($args);
                while($wp->have_posts()): $wp->the_post();
                ?>
                <li class="col-md-4">
                    <h4><?php the_title();?></h4>
                    <i class="date"><?php the_time('d F Y');?></i>
                    <?php 
                        if ( get_the_excerpt() ) :
                            the_excerpt();
                        else :
                            echo wp_trim_words(get_the_content(), 50);
                        endif;
                    ?>
                    <a href="<?php the_permalink();?>">Read more</a>
                </li>
                <?php endwhile;wp_reset_query();?>
            </ul>
        </div>
    </div>
    <?php }?>
<?php get_footer();?>