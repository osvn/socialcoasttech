<?php
global $osvn_opt;
?>
    <footer>
        <div class="container relative">
            <div class="row">
                <div class="col-md-4 first">
                    <?php echo $osvn_opt['footer_text'];?>
                </div>
                <div class="col-md-4 second text-center">
                    <h2>Follow Us</h2>
                    <?php if(function_exists('osvn_social')){osvn_social();}?>
                </div>
                <div class="col-md-4 third">
                    <?php 
                            wp_nav_menu( array(
                                'container' => 'menu',
                                'container_class' => '',
                                'menu_class' => 'menu',
                                'menu_id' => 'footermenu',
                                'sort_column' => 'menu_order',
                                'theme_location' => 'primary',
                                'items_wrap'=>'<ul id="%1$s" class="%2$s" role="tablist">%3$s</ul>'
                            ));
                    ?>
                </div>
            </div>
            <div class="text-center copyright">&copy; <?php echo date('Y');?> Social Coast Technologies, Inc.</div>
        </div>
    </footer><!--/footer -->
    
</div>
<?php wp_footer();?>
<script><?php if(isset($osvn_opt['footer_script']) && !empty($osvn_opt['footer_script'])){echo $osvn_opt['footer_script'];} ?></script>
</body>
</html>
