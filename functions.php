<?php
// Define
define( 'OSVN_URI', get_template_directory_uri() );
define( 'OSVN_CSS', OSVN_URI . '/assets/styles' );
define( 'OSVN_JS', OSVN_URI . '/assets/scripts' );
define( 'OSVN_IMG', OSVN_URI . '/assets/images' );
define( 'OSVN_INC', OSVN_URI . '/inc' );
define( 'OSVN_LANG', OSVN_URI . '/lang' );

define( 'OSVN_URL', get_template_directory() );
define( 'OSVN_CSS_URL', OSVN_URL . '/assets/styles' );
define( 'OSVN_JS_URL', OSVN_URL . '/assets/scripts' );
define( 'OSVN_IMG_URL', OSVN_URL . '/assets/images' );
define( 'OSVN_INC_URL', OSVN_URL . '/inc' );
define( 'OSVN_LANG_URL', OSVN_URL . '/lang' );


if ( ! function_exists( 'osvn_setups' ) ) {

	function osvn_setups() {

		load_theme_textdomain( 'osvn', OSVN_LANG_URL );

		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );


		register_nav_menus( array(
			'primary' => __( 'Top menu', 'osvn' ),
			'secondary' => __( 'Footer menu', 'osvn' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
		) );
		add_theme_support( 'infinite-scroll', array(
			'type' => 'click',
		    'container' => 'columns2',
		    'wrapper' => false,
		    'render' => 'infinite_scroll_render',
		) );
		/*
		add_theme_support( 'post-formats', array(
			'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
		) );
		*/

		//add_theme_support( 'custom-background' );

		//add_theme_support( 'woocommerce' );
		//add image size
		add_image_size( 'home-slider', 745, 385, true );
		add_image_size( 'home-team', 192, 192, true );
		
	}

}
add_action( 'after_setup_theme', 'osvn_setups' );


function osvn_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'osvn' ),
		'id'            => 'p-sidebar',
		'description'   => __( 'Main sidebar', 'osvn' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer sidebar', 'osvn' ),
		'id'            => 's-footer',
		'description'   => __( 'Footer sidebar', 'osvn' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s col-md-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	

}

add_action( 'widgets_init', 'osvn_widgets_init' );


function osvn_scripts() {

	// Load our main stylesheet.
	wp_enqueue_style( 'osvn-reset', OSVN_CSS . '/reset.css', array(), '1.0' );
	wp_enqueue_style( 'osvn-class', OSVN_CSS . '/class.css', array(), '1.0' );
	wp_enqueue_style( 'osvn-awesome', OSVN_CSS . '/awesome.css', array(), '1.0' );
	wp_enqueue_style( 'osvn-bootstrap', OSVN_URI . '/assets/dist/css/bootstrap.min.css', array(), '1.1' );
	wp_enqueue_style( 'osvn-style', OSVN_URI . '/assets/style.css', array(), '1.0' );
	wp_enqueue_style( 'osvn-style-main', OSVN_URI . '/style.css', array(), '1.1' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'osvn-flexslider-js', OSVN_JS . '/jquery.flexslider-min.js', array( 'jquery' ), '1.0', false );
	wp_enqueue_script( 'osvn-scrollTo-js', OSVN_JS . '/jquery.scrollTo.min.js', array( 'jquery' ), '1.0', false );
	wp_enqueue_script( 'osvn-modernizr-js', OSVN_JS . '/modernizr.js', array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'osvn-main-js', OSVN_JS . '/main.js', array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'osvn-bootstrap-js', OSVN_URI . '/assets/dist/js/bootstrap.min.js', array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'osvn-ie10-viewport-bug-js', OSVN_URI . '/assets/dist/js/ie10-viewport-bug-workaround.js', array( 'jquery' ), '1.0', true );

	
}

add_action( 'wp_enqueue_scripts', 'osvn_scripts' );

//redux framework
if ( ! class_exists( 'ReduxFramework' ) && file_exists( OSVN_INC_URL . '/osvn_panel/framework.php' ) ) {
	require_once( OSVN_INC_URL . '/osvn_panel/framework.php' );
}
if ( file_exists( OSVN_INC_URL . '/osvn_panel/config.php' ) ) {
	require_once( OSVN_INC_URL . '/osvn_panel/config.php' );
}
if ( file_exists( OSVN_INC_URL . '/class-tgm-plugin-activation.php' ) ) {
	require_once( OSVN_INC_URL . '/class-tgm-plugin-activation.php' );
}

add_action( 'tgmpa_register', 'mytheme_require_plugins' );

function mytheme_require_plugins() {
	global $config;
	$plugins = array(
		array(
			'name'     => 'WordPress SEO by Yoast',
			'slug'     => 'wordpress-seo',
			'required' => false, // this plugin is recommended
		),
		array(
			'name'     => 'Newsletter',
			'slug'     => 'newsletter',
			'required' => true, // this plugin is recommended
		),
		array(
			'name'     => 'Contact Form 7',
			'slug'     => 'contact-form-7',
			'required' => true, // this plugin is recommended
		),
		
		array(
			'name'     => 'Duplicator',
			'slug'     => 'duplicator',
			'required' => false, // this plugin is recommended
		),
	);

	tgmpa( $plugins, $config );

}

require OSVN_INC_URL . '/oss_functions.php';

// Remove Admin bar
add_filter( 'show_admin_bar', '__return_false' );

// CH Dashboard Widget
add_action( 'wp_dashboard_setup', 'osvn_custom_dashboard_widgets' );

if ( file_exists( OSVN_INC_URL . '/rich_snippets/index.php' ) ) {
	require_once( OSVN_INC_URL . '/rich_snippets/index.php' );
}
//metabox
function osvn_initialize_cmb_meta_boxes() {
    if ( ! class_exists( 'cmb_Meta_Box' ) )
        require_once( OSVN_INC_URL . '/meta_box/init.php' );
}
add_action( 'init', 'osvn_initialize_cmb_meta_boxes', 9999 );


function be_metabox_post_format( $display, $meta_box ) {
    if ( 'post_format' !== $meta_box['show_on']['key'] )
        return $display;


    if( isset( $_GET['post'] ) ) $post_id = $_GET['post'];
    elseif( isset( $_POST['post_ID'] ) ) $post_id = $_POST['post_ID'];
    if( !isset( $post_id ) )
        return $display;

    $meta_box['show_on']['value'] = !is_array( $meta_box['show_on']['value'] ) ? array( $meta_box['show_on']['value'] ) : $meta_box['show_on']['value'];

    if ( in_array( get_post_format( $post_id ), $meta_box['show_on']['value'] ) )
        return true;
    else
        return false;
}
add_filter( 'cmb_show_on', 'be_metabox_post_format', 10, 2 );
require_once( OSVN_INC_URL . '/meta_box/options.php' );
//add post type
/*require_once( OSVN_INC_URL . '/osvn-posttype.php' );
//add widget
require_once( OSVN_INC_URL . '/osvn_widget_product_price.php' );
require_once( OSVN_INC_URL . '/osvn_widget_product_color.php' );*/
