jQuery(document).ready(function(){
	jQuery('.flexslider').flexslider({'controlNav' : false,'directionNav':false});
	jQuery('.anchor a').click(function(event){
		var a = jQuery(this).attr("href");
		event.preventDefault();
		jQuery('body').scrollTo( 
			a ,
			1000,
			{
				'offset': {
					left: 0, 
					top: -76
				}
			}
		);
	});
});
jQuery(window).bind("load", function() {
	jQuery('.navbar-main').height(jQuery('body').height());
	jQuery('.navbar-toggle').on( "click", function(){
		if (jQuery('.navbar-main').hasClass('active'))
			jQuery('.navbar-main').removeClass('active');
		else
			jQuery('.navbar-main').addClass('active');
	});
	jQuery('.navbar-main .close-btn').click(function(){
		jQuery('.navbar-main').removeClass('active');
		jQuery('#navbar').removeClass('in');
	});
});