							<div <?php echo post_class();?>>
                                <a href="<?php the_permalink();?>"><?php the_post_thumbnail('home-blog');?></a>
                                <p class="news-date"><?php the_time('M j');?></p>
                                <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                                <div>
                                    <?php 
                                    if ( !empty( $post->post_excerpt ) ) :
                                        the_excerpt();
                                    else :
                                        echo wp_trim_words($post->post_content, 50);
                                    endif;
                                    ?>
                                </div>
                                <a href="<?php the_permalink();?>" class="readmore">read more</a>
                            </div>