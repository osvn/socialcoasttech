<?php
get_header();
global $osvn_opt, $post;
?>
        <div class="main">
            <!-- /.page-caption -->
            <div class="container">
                <div class="row">
                    <div class="main-content col-md-8">
                        <div class="post-details">
                            <?php while(have_posts()): the_post();?>
                            <div class="post-header">
                                <?php the_post_thumbnail();?>
                                <h1 class="post-title"><?php the_title();?></h1>
                            </div>
                            <!-- /.post-header -->
                            <div class="post-content">
                                <?php the_content();?>
                            </div>
                            <!-- /.post-content -->
                            <!-- /.post-footer -->
                            <?php endwhile;?>
                        </div>
                        <!-- /.post-details -->
                    </div>
                    <!-- /.main-content -->
                    <?php get_sidebar();?>
                    <!-- /.sidebar -->
                </div>
            </div>
        </div>
        <!-- /.main -->
<?php get_footer();?>